export type FormulaValues = Record<string, {
    value?: number | number[] | undefined,
    unit: string,
}>
export type FormulaInput = Record<string, number | number[] | undefined>

export interface Formula {
    title: string;
    description: string;
    inputValues: FormulaValues;
    func: (inputValues: FormulaInput) => FormulaValues; 
}
