import type { Formula, FormulaInput, FormulaValues } from "../types/formula";
import { zip } from "../utils/zip";
import { zip_tris } from "../utils/zip_tris";

export const centerOfGravity: Formula = {
  title: "Areas geometry", //chiave: "valore",
  description:
    "Calculate the properties of a bi-dimensional distribution of concentrated areas",
  inputValues: {
    A_i: { unit: "square meters" },
    x_i: { unit: "meters" },
    y_i: { unit: "meters" },
  },
  func: function (inputValues: FormulaInput) {
    return {
      xg: {
        value:
          zip(inputValues.x_i as number[], inputValues.A_i as number[])
            .map(([xi, ai]) => xi * ai)
            .reduce((prev, curr) => prev + curr) /
          (inputValues.A_i as number[]).reduce((prev, curr) => prev + curr),
        unit: "meters",
      },
      yg: {
        value:
          zip(inputValues.y_i as number[], inputValues.A_i as number[])
            .map(([yi, ai]) => yi * ai)
            .reduce((prev, curr) => prev + curr) /
          (inputValues.A_i as number[]).reduce((prev, curr) => prev + curr),
        unit: "meters",
      },
      I_x: {
        value: zip(inputValues.y_i as number[], inputValues.A_i as number[])
          .map(([yi, ai]) => yi ** 2 * ai)
          .reduce((prev, curr) => prev + curr),
        unit: "meters 4",
      },
      I_y: {
        value: zip(inputValues.x_i as number[], inputValues.A_i as number[])
          .map(([xi, ai]) => xi ** 2 * ai)
          .reduce((prev, curr) => prev + curr),
        unit: "meters 4",
      },
      I_xy: {
        value: zip_tris(
          inputValues.x_i as number[],
          inputValues.y_i as number[],
          inputValues.A_i as number[]
        )
          .map(([xi, ai, yi]) => xi * yi * ai)
          .reduce((prev, mid, curr) => prev + mid + curr),
        unit: "meters 4",
      },
    };
  },
};
