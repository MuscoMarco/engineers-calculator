import type { Formula, FormulaInput, FormulaValues } from "../types/formula";

const chi = 110;
const s = 1.46 * 10 ** -6;

export const viscosity: Formula = {
    title: "Viscosity", //chiave: "valore",
    description: "Calculate the viscosity of air given its temperature",
    inputValues: {
        Temperature: {unit: "kelvin"},
    },
    func: function(inputValues: FormulaInput) {
        const temperature = inputValues.Temperature as number;
        return {
            mu: {value: (s * temperature ** 1.5) / (chi + temperature), unit: "Pa s"},
        };
    },
};
