import type { Formula } from "../types/formula";
import { centerOfGravity } from "./centerOfGravity";
import { rectangleArea } from "./rectangleArea";
import { viscosity } from "./viscosity";
import { isentropicPressure } from "./isentropicPressure";
import { isentropicTemperature } from "./isentropicTemperature";

export const formulas = {
  /* rectangleArea:  */ rectangleArea, //se chiave e valore hanno lo stesso nome posso scriverlo una volta sola
    centerOfGravity,
    viscosity,
    isentropicPressure,
    isentropicTemperature,
};

export type FormulaName = keyof typeof formulas;
