import type { Formula, FormulaInput, FormulaValues } from "../types/formula";

export const isentropicPressure: Formula = {
    title: "Isentropic relations: pressure", //chiave: "valore",
    description: "Calculate either mach number, static pressure or total pressure of a flow; two out of three parameters has to be inputted",
    inputValues: {
        Total_Pressure: {unit: "Pa"},
        Static_Pressure: {unit:"Pa"},
        Mach: {unit:"-"},
        Gamma: {unit:"-"},
    }, //la chiave è una stringa, il valore è un object
    func: function(inputValues: FormulaInput) {
        const totalP = inputValues.Total_Pressure as number;
        const staticP = inputValues.Static_Pressure as number;
        const mach = inputValues.Mach as number;
        const gamma = inputValues.Gamma as number;
        if (totalP === undefined) {
            return{
                totalP: {value: staticP*(1+((gamma-1)/2)*mach**2)**(gamma/(gamma-1)), unit: "Pa"},
            }
        }
        if (staticP === undefined) {
            return{
                staticP: {value: totalP*(1+((gamma-1)/2)*mach**2)**(-gamma/(gamma-1)) , unit: "Pa"},
            }
        }
        if (mach === undefined) {
         return{
                mach: {value: (((totalP/staticP)**((gamma-1)/gamma) -1)/((gamma-1)/2))**(0.5), unit: "-"},
            }
        }
    }
}
