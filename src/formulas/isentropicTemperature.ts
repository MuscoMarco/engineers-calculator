import type { Formula, FormulaInput, FormulaValues } from "../types/formula";

export const isentropicTemperature: Formula = {
    title: "Isentropic relations: temperature", //chiave: "valore",
    description: "Calculate either mach number, static temperature or total temperature of a flow; two out of three parameters has to be inputted",
    inputValues: {
        Total_Temperature: {unit: "Kelvin"},
        Static_Temperature: {unit:"Kelvin"},
        Mach: {unit:"-"},
        Gamma: {unit:"-"},
    },
    func: function(inputValues: FormulaInput) {
        const totalT = inputValues.Total_Temperature as number;
        const staticT = inputValues.Static_Temperature as number;
        const mach = inputValues.Mach as number;
        const gamma = inputValues.Gamma as number;
        if (totalT === undefined) {
            return{
                totalT: {value: staticT*(1+((gamma-1)/2)*mach**2), unit: "Kelvin"},
            }
        }
        if (staticT === undefined) {
            return{
                staticT: {value: totalT*(1+((gamma-1)/2)*mach**2)**(-1) , unit: "Kelvin"},
            }
        }
        if (mach === undefined) {
         return{
                mach: {value: ((totalT/staticT -1)/((gamma-1)/2))**(0.5), unit: "-"},
            }
        }
    }
}
