import type { Formula, FormulaInput, FormulaValues } from "../types/formula";

export const rectangleArea: Formula = {
    title: "Rectangle Area", //chiave: "valore",
    description: "Calculate the area of a rectangle given its base and height",
    inputValues: {
        Base: {unit: "meters"},
        Height: {unit:"meters"},
    }, //la chiave è una stringa, il valore è un object
    func: function(inputValues: FormulaInput) {
        return {
            area: {value: (inputValues.Base as number) * (inputValues.Height as number), unit: "square meters"}
        }
    }
}
